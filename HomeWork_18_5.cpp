#include <iostream>
#include <string>

class Player 
{
private:
    std::string name;
    int score;

public:
    // ����������� ������ Player
    Player(const std::string& playerName, int playerScore) : name(playerName), score(playerScore) {}

    // ������ ������� � ������ ������
    std::string getName() const { return name; }
    int getScore() const { return score; }
};

// ������� ���������� ������� ������� �� �������� ���������� �����
void sortPlayers(Player** players, int size) 
{
    for (int i = 0; i < size - 1; ++i) 
    {
        for (int j = 0; j < size - i - 1; ++j) 
        {
            if (players[j]->getScore() < players[j + 1]->getScore()) 
            {
                std::swap(players[j], players[j + 1]);
            }
        }
    }
}

int main() 
{
    int numPlayers;
    std::cout << "How many players do you want to add? ";
    std::cin >> numPlayers;

    // �������� ������� ���������� �� ������� ������ Player
    Player** players = new Player * [numPlayers];

    // ��������� �� ������������ ���������� �� ������� � �������� �������� Player
    for (int i = 0; i < numPlayers; ++i) 
    {
        std::string playerName;
        int playerScore;

        std::cout << "Enter player name " << i + 1 << ": ";
        std::cin >> playerName;
        std::cout << "Enter the number of points for the player " << playerName << ": ";
        std::cin >> playerScore;

        players[i] = new Player(playerName, playerScore);
    }

    // ���������� ������� �� �������� �����
    sortPlayers(players, numPlayers);

    std::cout << "Player name | Glasses" << std::endl;
    for (int i = 0; i < numPlayers; ++i) 
    {
        std::cout << players[i]->getName() << "   |   " << players[i]->getScore() << std::endl;
    }

    for (int i = 0; i < numPlayers; ++i) 
    {
        delete players[i];
    }
    delete[] players;

    return 0;
}
